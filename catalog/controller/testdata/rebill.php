<?php
/**
 * Created by PhpStorm.
 * User: pingo
 * Date: 15.1.20
 * Time: 14:36
 */

class ControllerTestdataRebill extends Controller {

    public function index(){

        $start    = ''; //request
        $end      = ''; //request

        $data     = $this->getRebills($start, $end);

        header('Content-Type: application/json;charset=utf-8');

        echo json_encode($data);

        /*  output json
        {
          "rebillcount" : 0,
          "totalsum"    : 0,
          "error"       : false,
          "message"     : ""
        }

        */

        die();

    }


    private function getRebills($start, $end){

        $this->load->model('testdata/rebill');

        return $this->model_testdata_rebill->getData($start, $end);

    }

}
